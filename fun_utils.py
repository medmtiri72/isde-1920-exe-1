def load_mnist(csv_filename):
    """Load the MNIST dataset."""
    raise NotImplementedError


def split_data(x, y, fract_tr=0.5):
    """Split the data X,y into two random subsets."""
    raise NotImplementedError


class NMC(object):

    def __init__(self):
        self._centroids = None

    @property
    def centroids(self):
        return self._centroids

    @centroids.setter
    def centroids(self, value):
        self._centroids = value

    def fit(self, x, y):
        """Fitting the NMC classifier to data by
        estimating centroids for each class."""

        """Fitting the NMC classifier to data by
          estimating centroids for each class."""
        num_classes = np.unique(y).size
        num_features = x.shape[1]  # x.shape => (9999, 784)
        centroids = np.zeros(shape=(num_classes, num_features))  # (10, 784)
        for k in range(num_classes):
            centroids[k, :] = x[y == k, :].mean(axis=0)
        return centroids



        raise NotImplementedError

    def predict(self, x):
        """Predict the class of each input."""

        dist = euclidean_distances(x, centroids, squared=True)
        y_pred = np.argmin(dist, axis=1)
        return y_pred

        raise NotImplementedError
